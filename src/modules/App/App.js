import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Container } from 'reactstrap';

import style from './App.css';

import Header from './../../components/Header/Header'

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

export const mapState = (state, props) => ({ });
export const mapActions = (dispatch, props) => ({
    appReady: () => dispatch({type: 'cwTodo/APP_READY'}),
});

class App extends Component {
    static propTypes = {
        routes: PropTypes.array,
        params: PropTypes.object,

        appReady: PropTypes.func
    };

    componentWillMount() { }

    componentDidMount() {
        this.props.appReady();
    }

    componentWillUpdate(props) { }

    render() {
        return (
            <section className={style.root} >
                <Container fluid>
                    <Header />
                    <div className={style.page}>
                        { React.cloneElement(this.props.children, this.props) }
                    </div>
                </Container>
            </section>
        );
    }
}

export default connect(mapState, mapActions)(App)
