// import React from 'react';
import R from 'ramda';
import createSagaMiddleware from 'redux-saga';
  import {createStore, applyMiddleware, compose} from 'redux';

import reducers from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  const store = createStore(reducers, compose(
    applyMiddleware(sagaMiddleware),
    // this code allows to use redux dev tools chrome extension
    typeof window === 'object' &&
    typeof window.devToolsExtension !== 'undefined' ?
      // initialize, if there is a redux dev tools chrome extension
      window.devToolsExtension() :
      // otherwise, do nothing
      R.identity
  ));

  store.runSaga = sagaMiddleware.run(rootSaga);
  return store;
}
