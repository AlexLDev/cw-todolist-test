import { combineReducers } from 'redux';

import home from './home/home';

export const reducers = {
  home,
};

export default combineReducers(reducers);
