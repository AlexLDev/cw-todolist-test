import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Navbar, NavbarBrand } from 'reactstrap';

// import style from './Header.css';

export const mapState = (state, props) => ({
});
export const mapActions = (dispatch, props) => ({
});

class HeaderComponent extends Component {
    static propTypes = { };

    static defaultProps = { };

    render() {
        return (
            <header>
                <Navbar toggleable className="d-flex justify-content-between">
                    <NavbarBrand href="/">CW TODO List </NavbarBrand>
                </Navbar>
            </header>
        );
    }
}

export default connect(mapState, mapActions)(HeaderComponent)
