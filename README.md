This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Setup
1. `git clone git@bitbucket.org:AlexLDev/cw-todolist-test.git` or  `git clone https://AlexLDev@bitbucket.org/AlexLDev/cw-todolist-test.git`
2. `npm install`

## Run
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
