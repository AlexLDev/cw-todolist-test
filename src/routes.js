import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './modules/App/App';
import Home from './modules/Home/Home';

export default (store) => {
  return (
    <Route path="/" component={App}>
      <IndexRoute component={Home} name="ROOT" />
    </Route>
  );
}
