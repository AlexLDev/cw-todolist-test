import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ListGroup } from 'reactstrap';

import TodoItem from '../TodoItem/TodoItem';

// import style from './TodoList.css';

export default class TodoList extends Component {
    static propTypes = {
        completeTask: PropTypes.func,
        deleteTask: PropTypes.func,
    };

    componentDidMount() { };

    render() {
        const { data, completeTask, deleteTask } = this.props;

        return (
            <ListGroup>
                { data && data.map((el, key) => (
                        <TodoItem key={el.get('id')}
                                  data={el}
                                  deleteTask={deleteTask.bind(this, key)}
                                  completeTask={completeTask.bind(this, key)} />
                    )
                )}
            </ListGroup>
        );
    }
}
