import app from '../app/app';
import { immutableArray } from '../../libs/immutable/immutable';

import {take, call, put} from 'redux-saga/effects';

function * getTasks() {
  for(;;) {
    yield take('cwTodo/APP_READY');

    let { body: data } = yield call(app('/posts', 'get'));

    data = immutableArray(data);

    try {
        yield put({ type: 'cwTodo/TODO_LIST_LOADED', data });
    }
    catch(error) {
      console.log('Todo list load error', error);
    }
  }
}

function * completeTask() {
  for(;;) {
    const { data } = yield take('cwTodo/COMPLETE_TASK');

    try {
        yield put({ type: 'cwTodo/COMPLETE_TASK_LOADED', data });
    }
    catch(error) {
      console.log('Todo list load error', error);
    }
  }
}

function * deleteTask() {
  for(;;) {
    const { data } = yield take('cwTodo/DELETE_TASK');

    try {
        yield put({ type: 'cwTodo/DELETE_TASK_LOADED', data });
    }
    catch(error) {
      console.log('Todo list load error', error);
    }
  }
}

export default function * page() {
  yield [
    getTasks(),
    deleteTask(),
    completeTask(),
  ]
}

