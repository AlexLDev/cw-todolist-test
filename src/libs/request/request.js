import url from 'url';
import R from 'ramda';
import superagent from 'superagent-bluebird-promise';

const methods = ['get', 'post', 'put', 'delete', 'patch'];

const make = method => path => {
  return superagent[method](url.resolve('https://jsonplaceholder.typicode.com', path));
};

export default R.reduce((req, method) =>
  R.set(R.lensProp(method), make(method), req), {}, methods);
