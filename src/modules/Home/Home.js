import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import { Container, Row, Col } from 'reactstrap';

import TodoList from '../../components/TodoList/TodoList';

// import style from './Home.css';
export const mapState = (state, props) => ({
  tasks: state.home.tasks,
});
export const mapActions = (dispatch, props) => ({
  completeTask: payload => dispatch({
    type: 'cwTodo/COMPLETE_TASK',
    data: payload,
  }),
  deleteTask: payload => dispatch({
    type: 'cwTodo/DELETE_TASK',
    data: payload,
  }),
});

class Home extends Component {
    static propTypes = {
        completeTask: PropTypes.func,
        deleteTask: PropTypes.func,
    };

    handlerCompleteTask(id) {
        let { tasks, completeTask } = this.props;
        tasks = tasks.setIn([id, 'completed'], true);

        completeTask(tasks);
    }

    handlerDeleteTask(id) {
        let { tasks, deleteTask } = this.props;
        tasks = tasks.delete(id);

        deleteTask(tasks);
    }

    render() {
        const { tasks } = this.props;

        return (
            <Container fluid>
                <Row>
                    <Col xs="12">
                        <TodoList data={tasks}
                                  deleteTask={(id) => { this.handlerDeleteTask(id) }}
                                  completeTask={(id) => { this.handlerCompleteTask(id) }} />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default connect(mapState, mapActions)(Home)
