import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import configureStore from './store';
import Root from './root';


const renderToDomElement = document.getElementById('root'); // eslint-disable-line
const store = configureStore({}, browserHistory);

const render = () => {
    ReactDOM.render(
        <Root store={store}/>,
        renderToDomElement
    );

};

render();

