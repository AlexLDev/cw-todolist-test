import R from 'ramda';
import request from '../request/request';

export default (path, method, {query = {}, body = {}} = {}) => {
  const future = async () => {
    let req = request[method](`https://jsonplaceholder.typicode.com${path}`);

    if (!R.isEmpty(query)) {
      req = req.query(query);
    }
    if (!R.isEmpty(body)) {
      req = req.send(body);
    }

    return req;
  };

  future.method = method;
  future.path = path;
  future.query = query;
  future.body = body;
  return future;
};
