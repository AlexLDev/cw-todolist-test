import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import getRoutes from './routes';

const Root = ({store}) => (
  <Provider store={store} key="provider">
    <Router history={browserHistory}>
      {getRoutes(store)}
    </Router>
  </Provider>
);
Root.propTypes = {
  store: PropTypes.object.isRequired,
};
export default Root;