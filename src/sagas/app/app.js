import app from '../../libs/app/app';
import { call } from 'redux-saga/effects';

export default (path, method, {query = {}, body = {}} = {}) => {
  function * saga() {
    try {
      return yield call(app(path, method, {query, body}));
    } catch (err) {
      throw err;
    } finally {

    }
  }

  saga.method = method;
  saga.path = path;
  saga.query = query;
  saga.body = body;

  return saga;
};
