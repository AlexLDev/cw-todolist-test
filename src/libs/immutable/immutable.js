import { List, Map } from 'immutable';

export const immutableArray = (data) => {
    return List(
        data.map((el) => {
            el['completed'] = false;
            return Map(el);
        })
    );
};
