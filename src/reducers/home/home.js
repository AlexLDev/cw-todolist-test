import reducer from '../../libs/reredux/reducer';

export const initialState = {
  tasks: []
};

export default reducer(initialState, [

  ['cwTodo/TODO_LIST_LOADED', ({data}, state) => ({
    ...state,
    tasks: data
  })],

  ['cwTodo/COMPLETE_TASK_LOADED', ({data}, state) => ({
    ...state,
    tasks: data
  })],

  ['cwTodo/DELETE_TASK_LOADED', ({data}, state) => ({
    ...state,
    tasks: data
  })]

]);
