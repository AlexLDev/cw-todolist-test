import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ListGroupItem, ButtonGroup, Button } from 'reactstrap';

// import style from './TodoItem.css';

export default class TodoItem extends Component {
    static propTypes = {
        completeTask: PropTypes.func,
        deleteTask: PropTypes.func,
    };

    componentDidMount() { };

    render() {
        const { data, completeTask, deleteTask } = this.props;

        return (
            <ListGroupItem className="flex-row d-flex justify-content-between">
                { data.get('title') } - { data.get('completed')}
                <ButtonGroup className="pull-right">
                    { !data.get('completed') &&
                        <Button color="primary" disabled={data.get('completed')} onClick={completeTask}>Complete</Button>
                    }
                    <Button color="danger" onClick={deleteTask}>Delete</Button>
                </ButtonGroup>
            </ListGroupItem>
        );
    }
}
