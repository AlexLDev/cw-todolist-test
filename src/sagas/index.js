import app from './app/app';
import home from './home/home';

export default function * rootSaga() {
  yield [
    app(),
    home(),
  ];
}
